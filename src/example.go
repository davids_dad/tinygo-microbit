package main

import (
	"machine"
	"device/nrf"
	"time"
)

const (
        LED_COL_1 = 4
        LED_COL_2 = 5
        LED_COL_3 = 6
        LED_COL_4 = 7
        LED_COL_5 = 8
        LED_COL_6 = 9
        LED_COL_7 = 10
        LED_COL_8 = 11
        LED_COL_9 = 12
        LED_ROW_1 = 13
        LED_ROW_2 = 14
        LED_ROW_3 = 15
)

var rowMap = [...]uint8{
	LED_ROW_1,
	LED_ROW_2,
	LED_ROW_3,
}


var matrixSettings = [5][5][2]uint8{
	{{0, LED_COL_1}, {1, LED_COL_4}, {0, LED_COL_2}, {1, LED_COL_5}, {0, LED_COL_3}},
	{{2, LED_COL_4}, {2, LED_COL_5}, {2, LED_COL_6}, {2, LED_COL_7}, {2, LED_COL_8}},
	{{1, LED_COL_2}, {0, LED_COL_9}, {1, LED_COL_3}, {2, LED_COL_9}, {1, LED_COL_1}},
	{{0, LED_COL_8}, {0, LED_COL_7}, {0, LED_COL_6}, {0, LED_COL_5}, {0, LED_COL_4}},
	{{2, LED_COL_3}, {1, LED_COL_7}, {2, LED_COL_1}, {1, LED_COL_6}, {2, LED_COL_2}}}



var matrix [3]uint16

func clearMatrix() {
	for i := 0; i<3; i++ {
		matrix[i]=0x0000
	}
}

func showMatrix() {
	const AllColOff uint16 = 0x1ff0
	for row := 0; row <= 2; row++ {
		var rowBit uint16 = 1 << rowMap[row]
		var colBits uint16 = matrix[row]
		var bits uint16 = rowBit | (AllColOff ^ colBits)
		
		nrf.GPIO.OUT = nrf.RegValue(bits)
		time.Sleep(time.Millisecond * 1)
	}
}

func setPoint(x,y int ,value bool) {
	row := matrixSettings[y][x][0]
	col := matrixSettings[y][x][1]

	if value {
		matrix[row] |= 1 << uint8(col)
	}else{
		matrix[row] &^= (1 << uint8(col))
	}
}


func main() {
	machine.InitLEDMatrix()
	clearMatrix()

	for {
		for v := 0; v < 2; v++ {
			for y := 0; y < 5; y++ {
				for x := 0; x < 5; x++ {
					setPoint(x,y,true)
					for d := 0; d <100; d++ {
						showMatrix()
					}
					setPoint(x,y,false)
				}
			}
		}
	}
}
